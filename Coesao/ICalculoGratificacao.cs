namespace palestra.Coesao
{
    public interface ICalculoGratificacao
    {
        double Calcula(Funcionario funcionario);
    }
}
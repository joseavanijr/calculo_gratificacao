namespace palestra.Coesao
{
    public class Funcionario
    {
        public double SalarioBase { get; set; }
        public Cargo Cargo { get; set; }
        public Funcionario(Cargo cargo, double salarioBase)
        {
            this.Cargo = cargo;
            this.SalarioBase = salarioBase;
        }

        public double CalculaSalarioBruto(){
            return this.Cargo.Gratificacao.Calcula(this);
        }
    }
}
namespace palestra.Coesao
{
    public class CalculoRegraCursosTecnicos : ICalculoGratificacao
    {
        public double Calcula(Funcionario funcionario)
        {
            return funcionario.SalarioBase + (funcionario.SalarioBase * 0.1);
        }
    }
}
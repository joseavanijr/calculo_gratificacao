namespace palestra.Coesao
{
    public class CalculoRegraPlanejamento : ICalculoGratificacao
    {
        public double Calcula(Funcionario funcionario)
        {
            return funcionario.SalarioBase + (funcionario.SalarioBase * 0.25);
        }
    }
}
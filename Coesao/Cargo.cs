namespace palestra.Coesao {
    public abstract class Cargo {
        public ICalculoGratificacao Gratificacao { get; set; }
        public Cargo (ICalculoGratificacao gratificacao) {
            this.Gratificacao = gratificacao;
        }
    }
}
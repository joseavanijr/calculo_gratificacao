namespace palestra.SemCoesao
{
    public class Funcionario
    {
        public string Cargo { get; set; }
        public double SalarioBase { get; set; }
        public Funcionario(string cargo, double salarioBase)
        {
            this.Cargo = cargo;
            this.SalarioBase = salarioBase;
        }
    }
}
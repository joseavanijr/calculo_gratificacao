namespace palestra.SemCoesao
{
    public class CalculadoraSalario
    {
        //Se for ANALISTA tem a gratificação de Cursos Técnicos na área, MAS se for PROFESSOR, tem a gratificação de Planejamento
        public double Calcula(Funcionario funcionario){
            if(funcionario.Cargo.ToUpper().Equals("ANALISTA")){
                return funcionario.SalarioBase + (funcionario.SalarioBase * 0.1);  
            }
            else if(funcionario.Cargo.ToUpper().Equals("PROFESSOR")){
                return funcionario.SalarioBase + (funcionario.SalarioBase * 0.25); 
            }

            return funcionario.SalarioBase;
        }
    }
}
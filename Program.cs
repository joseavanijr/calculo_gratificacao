﻿using System;
using palestra.Coesao;

namespace palestra
{
    class Program
    {
        static void Main(string[] args)
        {  
            Funcionario analista = new Funcionario(new Analista(new CalculoRegraCursosTecnicos()), 1500);
            Funcionario professor = new Funcionario(new Professor(new CalculoRegraPlanejamento()), 1500);

            Console.WriteLine($"O Analista tem um salário Bruto de: {analista.CalculaSalarioBruto()}");
            Console.WriteLine($"O Professor tem um salário Bruto de: {professor.CalculaSalarioBruto()}");
        }
    }
}
